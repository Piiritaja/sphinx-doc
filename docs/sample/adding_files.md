## Adding files
To include your own files to this page simply add them in the docs directory.
Sphinx supports both `md` and `rst` files.

After adding a new file its path needs to be specified in `index.rst` like so:
![new_file](new_file.png)