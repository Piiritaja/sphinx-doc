# Setup

## Page info

Page title, author, copyright and other info about the page can be changed in the `conf.py` file.
![page_settings](project_conf.png)

## Deploying to pages
Every commit made to master will generate new html files and after ~10min your new html files are presented on the page.
If you wish to deploy pages from a different branch, simply edit this line in the `.gitlab-ci.yml`:
![change branches](branches.gif)
