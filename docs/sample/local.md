# Generating HTML files locally

To generate html files locally on your computer you need to have [Sphinx](https://www.sphinx-doc.org/en/master/)
installed:
```
pip install -U Sphinx
```

Html files are generated using `sphinx-build` command. You need to specify the location of your doc files and location 
to store your html files. 
 
 This example is assuming that your doc files are in `docs` directory and html files will be outputed to `_build`
 directory.
```
sphinx-build -b html docs _build
```

Now you can run `index.html` in the `_build` directory.