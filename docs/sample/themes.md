# Themes

Sphinx supports many different themes for your page.

Most of them are listed [here](https://sphinx-themes.org/)

## Installing themes
Themes can be installed using pip.

If you find a theme you like, simply install it to your computer using command line:
```
pip install [theme_name]
```

This page uses `sphinx_rtd_theme` and you would install it like so:
```
pip install sphinx-rtd-theme
```

## Adding theme to your page

To add a theme to your page that you have installed on your computer, it needs to be specified in `con.py`.
 - Import the theme `imprt [theme name]`
 - Add the theme to extensions list `extensions = [theme_name]`
 - Specify the theme: `html_theme = "sphinx_rtd_theme"`

Theme configurations for this page:
![theme_conf](theme_conf.png)