## Some explanation:

- Add new/change `.rst` or `.md` files under `docs` directory

- new files still need to be added to `index.rst`.

- After every commit to master html files are re-generated from files under `docs` directory.

- If a file included in `index.rst` can't be found, sphinx will just ignore it.

- Changing rst file paths will not automatically update `index.rst`. Needs to be changed manually.